#!/bin/bash
/usr/local/bin/grip README.md --export readme.html
/usr/local/bin/grip SETUP.md --export setup.html
/usr/local/bin/grip FEEDS.md --export feeds.html
/usr/local/bin/grip CHANGELOG.md --export changelog.html